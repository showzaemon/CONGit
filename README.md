CONGit
===============

####CONGit is a free open source software for the beginner of Git. It saves your time for getting to use Git by yourself. To use Git, First, you need to learn about concept of entities and operations for Git. Next, using GUI software of Git, You need to learn which command of Git is invoked when clicking an object in it. Using CUI of Git, you need to learn syntaxes of Git command. CONGit gets rid of the time of learning them. 

1. Features
----------------
In single page, you can do followings. BTW, We call The user interface of CONGit 'EUI(Even User Interface)'. There is 'EUI' between 'CUI' and 'GUI'.

+ Select proper git command as clicking what you want on project's life cycle view.
+ Place the selected command automatically to the console view, edit and execute the command.
+ Read document of the selected command in the description view..

### 1.1 Screen shot
![Screen Shot](app/images/top_screen.png "Screen Shot")

2. Tested Environment
----------------
 This software has been tested following environment.

+ ubuntu	14.04 LTS
+ nvm 		 0.25.4
+ node.js  0.12.7
+ npm 		 2.13.2
+ chrome	44

3. Installation
----------------
Before following operations, you must install 'node.js', 'npm' and Browser with javascript engine.  Using 'nvm' is recommended. ([How to install node.js and npm](https://www.digitalocean.com/community/tutorials/how-to-install-node-js-on-an-ubuntu-14-04-server))

1. Change current directory to the one where you will install CONGit.
2. Download [an archive file](https://gitlab.com/showzaemon/CONGit/repository/archive.zip) and extract it.
3. `npm install`
4. Set up 'startup application' to execute `congitServer.js`.

### 3.1 options
 You can change these values in 'config.json'.

1. Initial working directory. A default value is '~'.  
2. Port number on service. A default value is 8087.

4. Usage
----------------

1. Access 'localhost:8087' by browser.
2. Function key F6 : focus console view. 

5. License
----------------
 Copyright (c) 2015, Takeshi Hotta

 [![Creative Commons License](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/) 
 This work is licensed under a [Creative Commons Attribution-NonCommercial 4.0 International License](http://creativecommons.org/licenses/by-nc/4.0/).
