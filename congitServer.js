#!/usr/bin/env node
/**
 * term.js
 * Copyright (c) 2012-2013, Christopher Jeffrey (MIT License)
 */

 /*jshint node: true */
  'use strict';

var
  http = require('http'),
	express = require('express'),
	io = require('socket.io'),
	pty = require('pty.js'),
	terminal = require('term.js'),
// basicAuth = require('basic-auth-connect'),
  fs = require('fs-ext'),
	header = '[congit]';

/**
 * Load config.json
 */
var
	config = JSON.parse(fs.readFileSync(__dirname + '/config.json', 'utf8')),
	port = config.port;

function getHomeDir(dir) {
  return (dir) ? dir : process.env.HOME;
}

process.title = 'congitServer.js';

/**
 * Confirm this server runs single.
 */
console.log(header + 'Booting server.');
var fd = fs.openSync(__filename, 'r');
fs.flock(fd, 'exnb', function(err) {
  if (err) {
    console.log(header + 'Another server is already running. Shutting down this.');
    process.exit(1);
  }

	/**
	 * Dump
	 */

	var stream;
	if (process.argv[2] === '--dump') {
	  stream = fs.createWriteStream(__dirname + '/dump.log');
	}

	/**
	 * Open Terminal
	 */

	var
	  buff = [],
		socket,
	  term;

	term = pty.fork(process.env.SHELL || 'sh', [], {
	  name: fs.existsSync('/lib/terminfo/x/xterm-256color') ? 'xterm-256color' : 'xterm',
	  cols: 80,
	  rows: 16,
	  cwd: getHomeDir(config.initDir)
	});

	term.on('data', function (data) {
	  if (stream) {
			stream.write('OUT: ' + data.toString() + '\n-\n');
		}
	  return !socket ? buff.push(data) : socket.emit('data', data);
	});

	console.log(header + 'Created shell with pty master/slave pair (master: %d, pid: %d)',
	  term.fd, term.pid);

	/**
	 * App & Server
	 */

	var app = express(),
	  server = http.createServer(app);

	app.use(function (req, res, next) {
	  var setHeader = res.setHeader;
	  res.setHeader = function (name) {
	    switch (name) {
			case 'Cache-Control':
			case 'Last-Modified':
			case 'ETag':
	      return;
	    }
	    return setHeader.apply(res, arguments);
	  };
	  next();
	});

	/*
	app.use(basicAuth(function(user, pass, next) {
	  if (user !== 'foo' || pass !== 'bar') {
	    return next(true);
	  }
	  return next(null, user);
	}));
	*/

	app.use(express.static(__dirname));

	app.use(terminal.middleware());

	if (!~process.argv.indexOf('-n')) {
	  server.on('connection', function (socket) {
	    var address = socket.remoteAddress;
	    if (!address.match(/.*127.0.0.1$/) && !address.match(/^::1.*/)) {
	      try {
	        socket.destroy();
	      } catch (e) { }
	      console.log(header + 'Attempted connection from %s. Refused.', address);
	    }
	  });
	}

	server.listen(port);
	console.log(header + 'Lisning port is %d.', port);

	/**
	 * Sockets
	 */

	io = io.listen(server, {
	  log: false
	});
	var order = 0; // count a connection.
	io.sockets.on('connection', function (sock) {
	  socket = sock;

	  socket.on('data', function (data) {
	    if (stream) {
				stream.write('IN: ' + data.toString() + '\n-\n');
			}
	    if (data) {
				term.write(data);
			}
	  });

	  socket.on('disconnect', function () {
	    socket = null;
	  });

		socket.on('resize', function (size) {
	    term.resize(size.x, size.y);
	  });

	  socket.on('shutdown', function () {
			console.log(header + 'Shuting down');
	    process.exit();
	  });

	  socket.on('config', function (data) {
			config.initDir = data.initDir;
			fs.writeFile(__dirname + '/config.json', JSON.stringify(config), function (err) {
				if (err) {
					throw err;
				}
				term.write('cd ' + getHomeDir(config.initDir) + '\r');
			});
		});
			
		while (buff.length) {
	    socket.emit('data', buff.shift());
	  }
		
		socket.emit('order', order++);

		socket.emit('config', config);
		
	});

}); // locked block.
