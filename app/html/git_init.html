<div id="main">
	<div class="sect1">
		<h2 id="_name">NAME</h2>
		<div class="sectionbody"><div class="paragraph">
			<p>git-init - Create an empty Git repository or reinitialize an existing one</p>
			</div></div>
	</div>

	<div class="sect1">
		<h2 id="_synopsis">SYNOPSIS</h2>
		<div class="sectionbody"><div class="verseblock">
			<pre class="content">'git init' [-q | --quiet] [--bare] [--template=&lt;template_directory&gt;]
	  [--separate-git-dir &lt;git dir&gt;]
	  [--shared[=&lt;permissions&gt;]] [directory]</pre>
			</div></div>
	</div>

	<div class="sect1">
		<h2 id="_description">DESCRIPTION</h2>
		<div class="sectionbody"><div class="paragraph">
			<p>This command creates an empty Git repository - basically a <code>.git</code>
				directory with subdirectories for <code>objects</code>, <code>refs/heads</code>,
				<code>refs/tags</code>, and template files.  An initial <code>HEAD</code> file that
				references the HEAD of the master branch is also created.</p>
			</div>
			<div class="paragraph">
				<p>If the <code>$GIT_DIR</code> environment variable is set then it specifies a path
					to use instead of <code>./.git</code> for the base of the repository.</p>
			</div>
			<div class="paragraph">
				<p>If the object storage directory is specified via the
					<code>$GIT_OBJECT_DIRECTORY</code> environment variable then the sha1 directories
					are created underneath - otherwise the default <code>$GIT_DIR/objects</code>
					directory is used.</p>
			</div>
			<div class="paragraph">
				<p>Running <em>git init</em> in an existing repository is safe. It will not
					overwrite things that are already there. The primary reason for
					rerunning <em>git init</em> is to pick up newly added templates (or to move
					the repository to another place if --separate-git-dir is given).</p>
			</div></div>
	</div>

	<div class="sect1">
		<h2 id="_options">OPTIONS</h2>
		<div class="sectionbody"><div class="openblock">
			<div class="content">
				<div class="dlist">
					<dl>
						<dt class="hdlist1">-q</dt>
						<dt class="hdlist1">--quiet</dt>
						<dd>
							<p>Only print error and warning messages; all other output will be suppressed.</p>
						</dd>
						<dt class="hdlist1">--bare</dt>
						<dd>
							<p>Create a bare repository. If GIT_DIR environment is not set, it is set to the
								current working directory.</p>
						</dd>
						<dt class="hdlist1">--template=&lt;template_directory&gt;</dt>
						<dd>
							<p>Specify the directory from which templates will be used.  (See the "TEMPLATE
								DIRECTORY" section below.)</p>
						</dd>
						<dt class="hdlist1">--separate-git-dir=&lt;git dir&gt;</dt>
						<dd>
							<p>Instead of initializing the repository as a directory to either <code>$GIT_DIR</code> or
								<code>./.git/</code>, create a text file there containing the path to the actual
								repository.  This file acts as filesystem-agnostic Git symbolic link to the
								repository.</p>
							<div class="paragraph">
								<p>If this is reinitialization, the repository will be moved to the specified path.</p>
							</div>
						</dd>
						<dt class="hdlist1">--shared[=(false|true|umask|group|all|world|everybody|0xxx)]</dt>
						<dd>
							<p>Specify that the Git repository is to be shared amongst several users.  This
								allows users belonging to the same group to push into that
								repository.  When specified, the config variable "core.sharedRepository" is
								set so that files and directories under <code>$GIT_DIR</code> are created with the
								requested permissions.  When not specified, Git will use permissions reported
								by umask(2).</p>
							<div class="paragraph">
								<p>The option can have the following values, defaulting to <em>group</em> if no value
									is given:</p>
							</div>
						</dd>
					</dl>
				</div>
			</div>
			</div>
			<div class="dlist">
				<dl>
					<dt class="hdlist1"><em>umask</em> (or <em>false</em>)</dt>
					<dd>
						<p>Use permissions reported by umask(2). The default, when <code>--shared</code> is not
							specified.</p>
					</dd>
					<dt class="hdlist1"><em>group</em> (or <em>true</em>)</dt>
					<dd>
						<p>Make the repository group-writable, (and g+sx, since the git group may be not
							the primary group of all users). This is used to loosen the permissions of an
							otherwise safe umask(2) value. Note that the umask still applies to the other
							permission bits (e.g. if umask is <em>0022</em>, using <em>group</em> will not remove read
							privileges from other (non-group) users). See <em>0xxx</em> for how to exactly specify
							the repository permissions.</p>
					</dd>
					<dt class="hdlist1"><em>all</em> (or <em>world</em> or <em>everybody</em>)</dt>
					<dd>
						<p>Same as <em>group</em>, but make the repository readable by all users.</p>
					</dd>
					<dt class="hdlist1"><em>0xxx</em></dt>
					<dd>
						<p><em>0xxx</em> is an octal number and each file will have mode <em>0xxx</em>. <em>0xxx</em> will
							override users' umask(2) value (and not only loosen permissions as <em>group</em> and
							<em>all</em> does). <em>0640</em> will create a repository which is group-readable, but not
							group-writable or accessible to others. <em>0660</em> will create a repo that is
							readable and writable to the current user and group, but inaccessible to others.</p>
					</dd>
				</dl>
			</div>
			<div class="openblock">
				<div class="content">
					<div class="paragraph">
						<p>By default, the configuration flag <code>receive.denyNonFastForwards</code> is enabled
							in shared repositories, so that you cannot force a non fast-forwarding push
							into it.</p>
					</div>
					<div class="paragraph">
						<p>If you provide a <em>directory</em>, the command is run inside it. If this directory
							does not exist, it will be created.</p>
					</div>
				</div>
			</div></div>
	</div>

	<div class="sect1">
		<h2 id="_template_directory">TEMPLATE DIRECTORY</h2>
		<div class="sectionbody"><div class="paragraph">
			<p>The template directory contains files and directories that will be copied to
				the <code>$GIT_DIR</code> after it is created.</p>
			</div>
			<div class="paragraph">
				<p>The template directory will be one of the following (in order):</p>
			</div>
			<div class="ulist">
				<ul>
					<li>
						<p>the argument given with the <code>--template</code> option;</p>
					</li>
					<li>
						<p>the contents of the <code>$GIT_TEMPLATE_DIR</code> environment variable;</p>
					</li>
					<li>
						<p>the <code>init.templateDir</code> configuration variable; or</p>
					</li>
					<li>
						<p>the default template directory: <code>/usr/share/git-core/templates</code>.</p>
					</li>
				</ul>
			</div>
			<div class="paragraph">
				<p>The default template directory includes some directory structure, suggested
					"exclude patterns" (see <a href="/docs/gitignore">gitignore[5]</a>), and sample hook files (see <a href="/docs/githooks">githooks[5]</a>).</p>
			</div></div>
	</div>

	<div class="sect1">
		<h2 id="_examples">EXAMPLES</h2>
		<div class="sectionbody"><div class="dlist">
			<dl>
				<dt class="hdlist1">Start a new Git repository for an existing code base</dt>
				<dd>
					<div class="listingblock">
						<div class="content">
							<pre>$ cd /path/to/my/codebase
$ git init      <b>(1)</b>
$ git add .     <b>(2)</b>
$ git commit    <b>(3)</b></pre>
						</div>
					</div>
					<div class="colist arabic">
						<ol>
							<li>
								<p>Create a /path/to/my/codebase/.git directory.</p>
							</li>
							<li>
								<p>Add all existing files to the index.</p>
							</li>
							<li>
								<p>Record the pristine state as the first commit in the history.</p>
							</li>
						</ol>
					</div>
				</dd>
			</dl>
			</div></div>
	</div>

	<div class="sect1">
		<h2 id="_git">GIT</h2>
		<div class="sectionbody"><div class="paragraph">
			<p>Part of the <a href="/docs/git">git[1]</a> suite</p>
			</div></div>
	</div>

</div>
