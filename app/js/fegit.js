define(["jquery", "bootstrap", "socket.io", "termjs"],
			 function (jquery, bootstrap, io, termjs) {
		'use strict';
	
		var
			socket,
			term,
			screen,
			maximize,
			lastPos,
			inEdit,
			config;
	
		function clear(size) {
			var i;
			for (i = size; i > 0; i--) {
				socket.emit("data", "\x08");
			}
		}
	
		screen = document.getElementById("console");
		inEdit = false;

		socket = io.connect();
		socket.on('connect', function () {
			term = new Terminal({
				cols: 80,
				rows: 16,
				useStyle: true,
				screenKeys: true,
				cursorBlink: false
			});
			term.open(screen);
//			term.write('\x1b[1m\x1b[31mWelcome to congit!\x1b[m\r\n');
			term.on('data', function (data) {
				if (inEdit && data === '\r') {
					inEdit = false;
				}
				socket.emit('data', data);
			});
			term.on('title', function (title) {
//				document.title = 'CONGit | ' + title;
			});
			socket.on('data', function (data) {
				term.write(data);
				if (!inEdit) {
					lastPos = term.x;
				}
			});
			socket.on('disconnect', function () {
				term.destroy();
			});
			socket.on('order', function (order) {
				if (order > 0) {
					clear(term.cols);
					socket.emit('data','\r');
				}
			});
			socket.on('config', function (data) {
				config = data;
				jquery('#initialDir').val(config.initDir);
			});
			maximize();
		});
	
		maximize = function () {
			term.element.style.width = '100%';
			term.element.style.height = '100%';
			
			var
				x = screen.clientWidth / term.element.offsetWidth,
				y = screen.clientHeight / term.element.offsetHeight,
				size = {};
			
			size.x = (x * term.cols) | 0;
			size.y = (y * term.rows) | 0;
			
			term.resize(size.x, size.y);
			socket.emit('resize', size);	
		};
		
		window.onresize = maximize;
		
		jquery(".command").click(function () {
			var element = document.getElementById("console");
			element.dataset.posX = lastPos;
			element.dataset.posY = term.y;
			var command = this.getAttribute("data-command");
			inEdit = true;
			clear(term.x - lastPos);
			socket.emit("data", command);
		});
		
		jquery("#shutdown").click(function () {
			socket.emit("shutdown");
		});
	
		jquery("#ok-preferences").click(function () {
			config.initDir = jquery('#initialDir').val();
			socket.emit("config", config);
		});
	
		jquery("body").keydown(function (e) {
			var code = e.keyCode || e.which;
			if (code === 117) { // kecode(f6) -> 117
				e.preventDefault();
				jquery(".terminal").focus();
			}
		});
		
	});
