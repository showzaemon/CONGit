requirejs.config({
	baseUrl:				"./",
	"paths": {
		"fegit":			"app/js/fegit",
		"jquery":			"lib/js/jquery-1.11.3.min",
		"bootstrap":	"lib/bootstrap/js/bootstrap.min",
		"socket.io":	"/socket.io/socket.io",
		"termjs":			"lib/js/term",
		"ttyjs":			"lib/js/tty"
	},
	"shim": {
		"bootstrap":	{ deps: ["jquery"] },
		"ttyjs":				{ deps: ["socket.io", "termjs"] }
	}
});

requirejs(["fegit"]);
