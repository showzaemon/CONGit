module.exports = {
  url: 'http://localhost:8087/',
  sections: {
    create_commands: {
      selector: '#collapseListGroup1',
      elements: {
        init_command: { 
          selector: 'a[href="app/html/git_init.html"]' 
        },
        clone_command: { 
          selector: 'a[href="app/html/git_clone.html"]' 
        }
      }
    },
    change_sources_commands: {
      selector: '#collapseListGroup2',
      elements: {
        status_command: { 
          selector: 'a[href="app/html/git_status.html"]' 
        },
        add_command: { 
          selector: 'a[href="app/html/git_add.html"]' 
        },
        rm_command: { 
          selector: 'a[href="app/html/git_rm.html"]' 
        },
        mv_command: { 
          selector: 'a[href="app/html/git_mv.html"]' 
        },
        commit_command: { 
          selector: 'a[href="app/html/git_commit.html"]' 
        }
      }
    },
    change_branches_commands: {
      selector: '#collapseListGroup3',
      elements: {
        branch_command: { 
          selector: 'li[data-command="git branch"] a' 
        },
        add_branch_command: { 
          selector: 'li[data-command="git branch xxxx"] a' 
        },
        checkout_command: { 
          selector: 'a[href="app/html/git_checkout.html"]' 
        },
        delete_branch_command: { 
          selector: 'li[data-command="git branch -d xx"] a' 
        },
        merge_command: { 
          selector: 'a[href="app/html/git_merge.html"]' 
        }
      }
    },
    synchronize_commands: {
      selector: '#collapseListGroup4',
      elements: {
        push_command: { 
          selector: 'a[href="app/html/git_push.html"]' 
        },
        pull_command: { 
          selector: 'a[href="app/html/git_pull.html"]' 
        }
      }
    },
    redo_log_commands: {
      selector: '#collapseListGroup5',
      elements: {
        undo_all_command: { 
          selector: 'li[data-command="git reset --hard HEAD"] a' 
        },
        undo_last_commit_command: { 
          selector: 'li[data-command="git reset --soft HEAD^"] a' 
        },
        undo_last_add_command: { 
          selector: 'li[data-command="git reset HEAD"] a' 
        },
        list_commit_logs_command: { 
          selector: 'a[href="app/html/git_log.html"]' 
        }
      }
    }
  },
  elements: {
    document: {
      selector: '#_name+div.sectionbody p'
    },
    terminal_wrapper: {
      selector: '#console'
    },
    focused: {
      selector: '*:focus'
    },
    terminal: {
      selector: '#console dvi.terminal'
    },
    lineNth: {
      selector: '#console div.terminal div:nth-child(_nth)'
    }
  }
};
