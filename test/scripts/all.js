module.exports = {
  'Test CONGit' : function (browser) {
  	var congit = browser.page.congit();
		var create_commands_section = congit.section.create_commands;
		var change_sources_commands_section = congit.section.change_sources_commands;
		var change_branches_commands_section = congit.section.change_branches_commands;
		var synchronize_commands_section = congit.section.synchronize_commands;
		var redo_log_commands_section = congit.section.redo_log_commands;

		function check_placed_commnad(command) {
			congit.getAttribute('@terminal_wrapper', 'data-pos-y', function (res) {
				var y = parseInt(res.value) + 1;
				var s = congit.elements.lineNth.selector.replace(/_nth/, y.toString());
				var c = '$ ' + command.replace(/-/, ' ');
				congit.assert.containsText(s, c);
			});
		}

		function check_document(keyword) {
			browser.frame('document');
			congit.waitForElementPresent('@document', 1000);
			congit.assert.containsText('@document', keyword);
			browser.frame(null);
		}

		function check_command_click(selector, keyword) {
			browser.click(selector);
			check_document(keyword);
			check_placed_commnad(keyword);
		}

		function check_create_section() {
			var s = create_commands_section;
			s.expect.element('@init_command').to.be.visible;
			s.expect.element('@clone_command').to.be.visible;
			check_command_click(s.elements.init_command.selector, 'git-init');
			check_command_click(s.elements.clone_command.selector, 'git-clone');
		};

		function check_change_sources_section() {
			var s = change_sources_commands_section;
			s.expect.element('@status_command').to.be.visible;
			s.expect.element('@add_command').to.be.visible;
			s.expect.element('@rm_command').to.be.visible;
			s.expect.element('@mv_command').to.be.visible;
			s.expect.element('@commit_command').to.be.visible;
			check_command_click(s.elements.status_command.selector, 'git-status');
			check_command_click(s.elements.add_command.selector, 'git-add');
			check_command_click(s.elements.rm_command.selector, 'git-rm');
			check_command_click(s.elements.mv_command.selector, 'git-mv');
			check_command_click(s.elements.commit_command.selector, 'git-commit');
		};

		function check_change_branches_section() {
			var s = change_branches_commands_section;
			s.expect.element('@branch_command').to.be.visible;
			s.expect.element('@add_branch_command').to.be.visible;
			s.expect.element('@checkout_command').to.be.visible;
			s.expect.element('@delete_branch_command').to.be.visible;
			s.expect.element('@merge_command').to.be.visible;
			check_command_click(s.elements.branch_command.selector, 'git-branch');
			check_command_click(s.elements.add_branch_command.selector, 'git-branch');
			check_command_click(s.elements.checkout_command.selector, 'git-checkout');
			check_command_click(s.elements.delete_branch_command.selector, 'git-branch');
			check_command_click(s.elements.merge_command.selector, 'git-merge');
		};

		function check_synchronize_section() {
			var s = synchronize_commands_section;
			s.expect.element('@push_command').to.be.visible;
			s.expect.element('@pull_command').to.be.visible;
			check_command_click(s.elements.push_command.selector, 'git-push');
			check_command_click(s.elements.pull_command.selector, 'git-pull');
		};

		function check_redo_log_section() {
			var s = redo_log_commands_section;
			s.expect.element('@undo_all_command').to.be.visible;
			s.expect.element('@undo_last_commit_command').to.be.visible;
			s.expect.element('@undo_last_add_command').to.be.visible;
			s.expect.element('@list_commit_logs_command').to.be.visible;
			check_command_click(s.elements.undo_all_command.selector, 'git-reset');
			check_command_click(s.elements.undo_last_commit_command.selector, 'git-reset');
			check_command_click(s.elements.undo_last_add_command.selector, 'git-reset');
			check_command_click(s.elements.list_commit_logs_command.selector, 'git-log');
		};

  	congit.navigate().assert.title('CONGit');
		congit.expect.section('@create_commands').to.not.be.visible;
		congit.expect.section('@change_sources_commands').to.be.visible;
		congit.expect.section('@change_branches_commands').to.not.be.visible;
		congit.expect.section('@synchronize_commands').to.not.be.visible;
		congit.expect.section('@redo_log_commands').to.not.be.visible;

		browser.click('a[href="#collapseListGroup1"]');
		congit.waitForElementVisible(create_commands_section.selector, 1000);
		check_create_section();

		congit.expect.section('@change_sources_commands').to.be.visible;
		check_change_sources_section();

		browser.click('a[href="#collapseListGroup3"]');
		congit.waitForElementVisible(change_branches_commands_section.selector, 1000);
		check_change_branches_section();

		browser.click('a[href="#collapseListGroup4"]');
		congit.waitForElementVisible(synchronize_commands_section.selector, 1000);
		check_synchronize_section();

		browser.click('a[href="#collapseListGroup5"]');
		congit.waitForElementVisible(redo_log_commands_section.selector, 1000);
		check_redo_log_section();

		// CHeck F6 key
		var rx = /\w+\@\w+:.+\$ git log/;
		congit.expect.element('@focused').text.to.not.match(rx);
		browser.keys(['\uE036'], function () {  // \uE036 is F6 key
			congit.expect.element('@focused').text.to.match(rx);
		});

		browser.end();
  }
};